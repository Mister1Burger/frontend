package com.example.freelanefront;


public class Utils {

    public static final int STATUS_ALL_OTHER_APPS = 111;
    public static final int STATUS_MY_APPS = 222;
    public static final int STATUS_CATEGORIES = 333;
    public static final int STATUS_EXPLORE = 444;
}
