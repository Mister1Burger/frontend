package com.example.freelanefront.adapters;


import android.content.Context;
import android.net.sip.SipSession;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.freelanefront.R;
import com.example.freelanefront.SquareImageView;

import java.io.Serializable;
import java.util.List;


public class AllOtherAppsAdapter extends RecyclerView.Adapter<AllOtherAppsAdapter.UserListViewHolder> implements Serializable {




public static class UserListViewHolder extends RecyclerView.ViewHolder implements Serializable {

    ImageView contentImage;
    SquareImageView logoImage;
    TextView appName;
    TextView otherInformation;
    TextView rating;
    ImageButton action;


    UserListViewHolder(View itemView) {
        super(itemView);
        contentImage = (ImageView) itemView.findViewById(R.id.content_image);
        logoImage = (SquareImageView) itemView.findViewById(R.id.logo_image);
        appName = (TextView) itemView.findViewById(R.id.app_name_tv);
        otherInformation = (TextView) itemView.findViewById(R.id.information_tv);
        rating = (TextView) itemView.findViewById(R.id.rate_tv);
        action = (ImageButton) itemView.findViewById(R.id.action_ib);
    }
}

    public AllOtherAppsAdapter(List<Object> content) {
        //TODO constructor with your Listener

    }


    @Override
    public AllOtherAppsAdapter.UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.apps_list_item, parent, false);
        UserListViewHolder pvh = new UserListViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(AllOtherAppsAdapter.UserListViewHolder holder, final int position) {

        //TODO content filling item

    }

    @Override
    public int getItemCount() {
        //TODO return list.size()
        return 0;
    }
}
