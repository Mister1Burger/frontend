package com.example.freelanefront.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.freelanefront.R;
import com.example.freelanefront.adapters.MyAppsAdapter;
import com.example.freelanefront.adapters.TrendsAdapter;

import java.util.List;


public class SearchFragment extends Fragment {

    TextView trending_tv;
    LinearLayout trending_item;
    RecyclerView contentList;
    TrendsAdapter adapter;
    List<Object> content;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        trending_tv = (TextView) view.findViewById(R.id.trending_tv);
        trending_item = (LinearLayout) view.findViewById(R.id.trending_item);
        contentList = (RecyclerView) view.findViewById(R.id.items_list);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        trending_tv.setVisibility(View.VISIBLE);
        //this for example view
        trending_item.setVisibility(View.VISIBLE);


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setStackFromEnd(true);
        contentList.setLayoutManager(llm);

        //get your content from MyFragmentManager
        content = MyFragmentManager.getInstance().getContentList();
        adapter = new TrendsAdapter(content);





    }

}
