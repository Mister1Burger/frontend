package com.example.freelanefront.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.freelanefront.AppDetailsActivity;
import com.example.freelanefront.MainActivity;
import com.example.freelanefront.R;
import com.example.freelanefront.adapters.AllOtherAppsAdapter;

import java.util.List;

public class AllOtherAppsFragment extends Fragment{

    LinearLayout firstAppsListItem;
    LinearLayout secondAppsListItem;
    RecyclerView contentList;
    AllOtherAppsAdapter adapter;
    List<Object> content;
    String fragmentTitle;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        firstAppsListItem = (LinearLayout) view.findViewById(R.id.apps_list_first_item);
        secondAppsListItem = (LinearLayout) view.findViewById(R.id.apps_list_second_item);
        contentList = (RecyclerView) view.findViewById(R.id.items_list);
        fragmentTitle = MyFragmentManager.getFragmentTitle();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //this for example view
        firstAppsListItem.setVisibility(View.VISIBLE);
        secondAppsListItem.setVisibility(View.VISIBLE);


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setStackFromEnd(true);
        contentList.setLayoutManager(llm);

        //get your content from MyFragmentManager
        content = MyFragmentManager.getInstance().getContentList();
        adapter = new AllOtherAppsAdapter(content);
        contentList.setAdapter(adapter);

        firstAppsListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AppDetailsActivity.class);
                startActivity(intent);
            }
        });

        secondAppsListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AppDetailsActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        if(!fragmentTitle.equals("")){
        ((MainActivity)getActivity()).getToolbarTitle().setText(fragmentTitle);}
    }
}
