package com.example.freelanefront.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.freelanefront.AppDetailsActivity;
import com.example.freelanefront.R;
import com.example.freelanefront.adapters.MyAppsAdapter;

import java.util.List;

public class DownloadedApps extends Fragment {

    LinearLayout firstMyAppItem;
    LinearLayout secondMyAppItem;
    RecyclerView contentList;
    MyAppsAdapter adapter;
    List<Object> content;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        firstMyAppItem = (LinearLayout) view.findViewById(R.id.my_app_first_item);
        secondMyAppItem = (LinearLayout) view.findViewById(R.id.my_app_second_item);
        contentList = (RecyclerView) view.findViewById(R.id.items_list);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //this for example view

        firstMyAppItem.setVisibility(View.VISIBLE);
        secondMyAppItem.setVisibility(View.VISIBLE);



        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setStackFromEnd(true);
        contentList.setLayoutManager(llm);

        //get your content from MyFragmentManager
        content = MyFragmentManager.getInstance().getContentList();
        adapter = new MyAppsAdapter(content);
        contentList.setAdapter(adapter);


        firstMyAppItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AppDetailsActivity.class);
                startActivity(intent);
            }
        });

        secondMyAppItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AppDetailsActivity.class);
                startActivity(intent);
            }
        });

    }
}
