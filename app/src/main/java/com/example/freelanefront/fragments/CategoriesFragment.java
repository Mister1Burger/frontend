package com.example.freelanefront.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.freelanefront.MainActivity;
import com.example.freelanefront.R;
import com.example.freelanefront.Utils;

public class CategoriesFragment extends Fragment {
    private LinearLayout firstCategory;
    private LinearLayout secondCategory;
    private LinearLayout thirdCategory;
    private LinearLayout fourthCategory;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories_fragment, container, false);
        firstCategory =(LinearLayout) view.findViewById(R.id.first_category);
        secondCategory = (LinearLayout) view.findViewById(R.id.second_category);
        thirdCategory = (LinearLayout) view.findViewById(R.id.third_category);
        fourthCategory = (LinearLayout) view.findViewById(R.id.fourth_category);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firstCategory.setOnClickListener(clickListener);
        secondCategory.setOnClickListener(clickListener);
        thirdCategory.setOnClickListener(clickListener);
        fourthCategory.setOnClickListener(clickListener);
    }
    View.OnClickListener clickListener = new View.OnClickListener() {


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.first_category:
                    //set toolbar title
                    MyFragmentManager.setFragmentTitle("Gaming");
                   MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                        break;

                case R.id.second_category:
                    MyFragmentManager.setFragmentTitle("Gaming");
                    MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                        break;

                case R.id.third_category:
                    MyFragmentManager.setFragmentTitle("Gaming");
                    MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                        break;

                case R.id.fourth_category:
                    MyFragmentManager.setFragmentTitle("Gaming");
                    MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                        break;

            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity)getActivity()).getToolbarTitle().setText("Categories");
    }
}
