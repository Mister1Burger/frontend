package com.example.freelanefront.fragments;


import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.example.freelanefront.R;
import com.example.freelanefront.Utils;


import java.util.ArrayList;
import java.util.List;

public class MyFragmentManager {

    List<Object> contentList;
    AllOtherAppsFragment allOtherAppsFragment;
    CategoriesFragment categoriesFragment;
    MyAppsFragment myAppsFragment;
    MyFragmentManager myFragmentManager;
    SearchFragment searchFragment;
    private static MyFragmentManager instance;
    private FragmentManager manager;
    private static int fragmentsCount = 0;
    private List<Integer> listFragments;
    private static String fragmentTitle = "";

    public static String getFragmentTitle() {
        return fragmentTitle;
    }

    public static void setFragmentTitle(String fragmentTitle) {
        MyFragmentManager.fragmentTitle = fragmentTitle;
    }

    public static MyFragmentManager getInstance() {
        if (instance == null) {
            instance = new MyFragmentManager();
        }
        return instance;
    }

    public void init(FragmentManager manager) {
        this.manager = manager;
        contentList = new ArrayList<>();
        listFragments = new ArrayList<>();
        searchFragment = new SearchFragment();
        myAppsFragment = new MyAppsFragment();
        allOtherAppsFragment = new AllOtherAppsFragment();
        categoriesFragment = new CategoriesFragment();
        myFragmentManager = new MyFragmentManager();
    }

    public static int getFragmentsCount() {
        return fragmentsCount;
    }

    public void fragmentsCountDecrease() {
        fragmentsCount--;
        listFragments.remove(getLastFragment());
    }

    private Integer getLastFragment(){
        if(listFragments.size() != 0){
        return listFragments.get(listFragments.size()-1);}
        else return 0;
    }

    public List<Object> getContentList() {
        return contentList;
    }

    public AllOtherAppsFragment getAllOtherAppsFragment() {
        return allOtherAppsFragment;
    }

    public CategoriesFragment getCategoriesFragment() {
        return categoriesFragment;
    }

    public MyFragmentManager getMyFragmentManager() {
        return myFragmentManager;
    }

    public void setContentList(List<Object> contentList) {
        this.contentList = contentList;
    }

    public void startFragment(int FragmentIdentificator) {
        if (manager != null) {

            switch (FragmentIdentificator) {
                case Utils.STATUS_CATEGORIES:
                    if (getLastFragment() != FragmentIdentificator) {
                        listFragments.add(FragmentIdentificator);
                        try {
                            fragmentsCount++;
                            manager.beginTransaction()
                                    .replace(R.id.fragment, categoriesFragment)
                                    .addToBackStack(null)
                                    .commit();
                        } catch (Exception e) {
                            Log.d("EXCEPTION", e.toString());
                        }
                    }
                    break;
                case Utils.STATUS_EXPLORE:
                    if (getLastFragment() != FragmentIdentificator) {
                        listFragments.add(FragmentIdentificator);
                    try {
                        fragmentsCount++;
                        manager.beginTransaction()
                                .replace(R.id.fragment, searchFragment)
                                .addToBackStack(null)
                                .commit();
                    } catch (Exception e) {
                        Log.d("EXCEPTION", e.toString());
                    }}

                    break;
                case Utils.STATUS_MY_APPS:
                    if (getLastFragment() != FragmentIdentificator) {
                        listFragments.add(FragmentIdentificator);
                        try {
                            fragmentsCount++;
                            manager.beginTransaction()
                                    .replace(R.id.fragment, myAppsFragment)
                                    .addToBackStack(null)
                                    .commit();
                        } catch (Exception e) {
                            Log.d("EXCEPTION", e.toString());
                        }
                    }
                    break;
                case Utils.STATUS_ALL_OTHER_APPS:
                    if (getLastFragment() != FragmentIdentificator) {
                        listFragments.add(FragmentIdentificator);
                        try {
                            fragmentsCount++;
                            manager.beginTransaction()
                                    .replace(R.id.fragment, allOtherAppsFragment)
                                    .addToBackStack(null)
                                    .commit();
                        } catch (Exception e) {
                            Log.d("EXCEPTION", e.toString());
                        }
                    }
                    break;
            }
        }
    }

}
