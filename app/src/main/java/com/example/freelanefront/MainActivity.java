package com.example.freelanefront;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.freelanefront.fragments.MyAppsFragment;
import com.example.freelanefront.fragments.MyFragmentManager;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener {


    private RapidFloatingActionLayout rfaLayout;
    private RapidFloatingActionButton rfaBtn;
    private RapidFloatingActionHelper rfabHelper;
    TextView toolbarTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);


        Toolbar toolbar = findViewById(R.id.toolbar);
        rfaLayout = findViewById(R.id.activity_main_rfal);
        rfaBtn = findViewById(R.id.activity_main_rfab);

        MyFragmentManager.getInstance().init(getSupportFragmentManager());
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_menu));

        NavigationView navigationView = findViewById(R.id.nav_view);
        createFloatingMenu(this);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.title);

        MyFragmentManager.getInstance().startFragment(Utils.STATUS_CATEGORIES);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else  if(MyFragmentManager.getFragmentsCount() == 1){
            finish();
        }else {
            MyFragmentManager.getInstance().fragmentsCountDecrease();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 /*TODO content request for example
                * MyFragmentManager.setContentList(content for this fragment)*/
                 MyFragmentManager.getInstance().startFragment(Utils.STATUS_EXPLORE);
            }
        });
        myActionMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener()
        {

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item)
            {
                // Do something when collapsed
                onBackPressed();
                return true; // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item)
            {
                // Do something when Expanded

                return true;
            }
        });
        return true;

    }




    private void createFloatingMenu(Context context) {
        RapidFloatingActionContentLabelList rfaContent = new RapidFloatingActionContentLabelList(context);
        rfaContent.setOnRapidFloatingActionContentLabelListListener(this);
        List<RFACLabelItem> items = new ArrayList<>();
        items.add(new RFACLabelItem<Integer>()
                .setLabel("My Apps")
                .setResId(R.drawable.something)
                //color of icon's background
                .setIconNormalColor(0xff0004ff)
                .setIconPressedColor(0xff0003c9)
                //text color
                .setLabelColor(0xff0004ff)
                .setLabelSizeSp(14)
                .setWrapper(Utils.STATUS_MY_APPS)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Categories")
                .setResId(R.drawable.house)
                .setIconNormalColor(0xff18cc00)
                .setIconPressedColor(0xff13a100)
                .setLabelColor(0xff18cc00)
                .setWrapper(Utils.STATUS_CATEGORIES)
        );

        rfaContent
                .setItems(items)
                .setIconShadowColor(0xff888888)
        ;
        rfabHelper = new RapidFloatingActionHelper(
                context,
                rfaLayout,
                rfaBtn,
                rfaContent
        ).build();


    }


    @Override
    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
        Toast.makeText(this, "clicked label: " + item.getWrapper(), Toast.LENGTH_SHORT).show();
        rfabHelper.toggleContent();
        switch ((int) item.getWrapper()) {
            case Utils.STATUS_CATEGORIES:
                /*TODO content request for example
                * MyFragmentManager.setContentList(content for this fragment)*/
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_CATEGORIES);
                break;
            case Utils.STATUS_EXPLORE:
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_EXPLORE);

                break;
            case Utils.STATUS_MY_APPS:
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_MY_APPS);
                break;
            case Utils.STATUS_ALL_OTHER_APPS:
                 MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                break;
        }
    }

    @Override
    public void onRFACItemIconClick(int position, RFACLabelItem item) {
        Toast.makeText(this, "clicked label: " + item.getWrapper(), Toast.LENGTH_SHORT).show();
        rfabHelper.toggleContent();
        switch ((int) item.getWrapper()) {
            case Utils.STATUS_CATEGORIES:
                 /*TODO content request for example
                * MyFragmentManager.setContentList(content for this fragment)*/
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_CATEGORIES);
                break;
            case Utils.STATUS_EXPLORE:
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_EXPLORE);

                break;
            case Utils.STATUS_MY_APPS:
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_MY_APPS);
                break;
            case Utils.STATUS_ALL_OTHER_APPS:
                MyFragmentManager.getInstance().startFragment(Utils.STATUS_ALL_OTHER_APPS);
                break;
        }
    }

    public TextView getToolbarTitle() {
        return toolbarTitle;
    }
}



