package com.example.freelanefront;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class AppDetailsActivity extends AppCompatActivity {

    SquareImageView logo_image;
    TextView information_tv;
    RatingBar logo_rating;
    TextView voted_times_logo_tv;
    ImageView peg_image_logo;
    ImageButton action_logo_ib;
    RecyclerView screenshots_list;
    TextView description_news;
    TextView rating_reviews_number;
    RatingBar rating_reviews_ratbar;
    TextView rating_reviews_downloaded;
    ProgressBar progress_five_star;
    ProgressBar progress_four_star;
    ProgressBar progress_three_star;
    ProgressBar progress_two_star;
    ProgressBar progress_one_star;
    RecyclerView comments_list;
    TextView update_date;
    TextView version_number;
    TextView what_interct_elements;
    TextView size_number;
    TextView version_android;
    TextView author_in_additional_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_detalis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Facebook");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        findViews();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void findViews() {
        logo_image = findViewById(R.id.logo_image);
        information_tv = findViewById(R.id.information_tv);
        logo_rating = findViewById(R.id.logo_rating);
        voted_times_logo_tv = findViewById(R.id.voted_times_logo_tv);
        peg_image_logo= findViewById(R.id.peg_image_logo);
        action_logo_ib= findViewById(R.id.action_logo_ib);
        screenshots_list= findViewById(R.id.screenshots_list);
        description_news= findViewById(R.id.description_news);
        rating_reviews_number= findViewById(R.id.rating_reviews_number);
        rating_reviews_ratbar= findViewById(R.id.rating_reviews_ratbar);
        rating_reviews_downloaded= findViewById(R.id.rating_reviews_downloaded);
        progress_five_star= findViewById(R.id.progress_five_star);
        progress_four_star= findViewById(R.id.progress_four_star);
        progress_three_star= findViewById(R.id.progress_three_star);
        progress_two_star= findViewById(R.id.progress_two_star);
        progress_one_star= findViewById(R.id.progress_one_star);
        comments_list= findViewById(R.id.comments_list);
        update_date= findViewById(R.id.update_date);
        version_number= findViewById(R.id.version_number);
        what_interct_elements= findViewById(R.id.what_interct_elements);
        size_number= findViewById(R.id.size_number);
        version_android= findViewById(R.id.version_android);
        author_in_additional_info = findViewById(R.id.author_in_additional_info);
    }

}